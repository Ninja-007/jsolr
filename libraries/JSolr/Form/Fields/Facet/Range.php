<?php
/**
 * @copyright   Copyright (C) 2013-2017 KnowledgeArc Ltd. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
namespace JSolr\Form\Fields\Facet;

/**
 * The Facets form field builds a list of facets which a user
 * can then apply to the current search result set to narrow their search
 * further (I.e. filter).
 */
class Range extends \JSolr\Form\Fields\Facet
{
    protected $type = 'JSolr.FacetRange';

    /**
     * (non-PHPdoc)
     * @see Facetable::getFacetQuery()
     */
    public function getFacetQuery()
    {
        $parent = parent::getFacetQuery();

        $facet = new \Solarium\Component\Facet\Range($parent->getOptions());

        $facet->setMinCount($parent->getMinCount());

        if ($this->start) {
            $facet->setStart($this->start);
        }

        if ($this->end) {
            $facet->setEnd($this->end);
        }

        if ($this->gap) {
            $facet->setGap($this->gap);
        }

        return $facet;
    }

    /**
     * (non-PHPdoc)
     * @see \JFormFieldList::getOptions()
     */
    protected function getOptions()
    {
        // Initialize variables.
        $options = array();

        $facets = $this->getFacet();

        foreach (array_reverse($facets->getValues()) as $key=>$value) {
            $html = array("<li>", "%s", "</li>");

            $key = \JSolr\Helper::getOriginalFacet($key);

            if ($this->isSelected($key)) {
                $html = array("<li class=\"active\">", "%s", "</li>");
            }

            $count = '';

            $date = \JFactory::getDate($key);

            if ($this->showcount === 'true') {
                $count = '<span class="facet-count">('.$value.')</span>';
            }

            $facet = '<a href="'.$this->buildFilterUri($key).'">'.$date->format('Y').'</a>'.$count;

            $options[] = \Joomla\CMS\Language\Text::sprintf(implode($html), $facet);
        }

        reset($options);

        return $options;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'start':
            case 'end':
            case 'gap':
                return $this->getAttribute($name, null);

                break;

            default:
                return parent::__get($name);
        }
    }
}
