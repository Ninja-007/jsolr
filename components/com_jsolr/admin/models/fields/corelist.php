<?php
/**
 * @copyright   Copyright (C) 2015-2017 KnowledgeArc Ltd. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

JFormHelper::loadFieldClass('list');

/**
 * Lists all enabled Solr cores.
 */
class JSolrFormFieldCoreList extends JFormFieldList
{
    protected $type = 'JSolr.CoreList';

	public function getOptions() {
        $params = JComponentHelper::getParams('com_jsolr', true);
        $options = [];

        $http = JHttpFactory::getHttp();

        $url = $params->get('url');

        if ($url && substr_compare($url, '/', -strlen('/')) !== 0) {
            $url .= '/';
        }

        // Solarium hardcodes the /solr path.
        $url = new JUri($url."solr/admin/cores?action=STATUS");

        // give up quickly.
        try {
            $response = $http->get((string)$url, null, 10);

            $data = json_decode($response->body);

            foreach ($data->status as $key=>$value) {
                $options[$key] = $key;
            }
        } catch (Exception $e) {
            JLoader::register('JSolrHelper', JPATH_ADMINISTRATOR . '/components/com_jsolr/helpers/jsolr.php');
            JSolrHelper::log($e->getMessage(), \JLog::ERROR);

            if (JDEBUG) {
                JSolrHelper::log($e->getTraceAsString(), \JLog::DEBUG);
            }
        }

        return $options;
	}
}
